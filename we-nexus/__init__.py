# -*- coding: utf-8 -*-
"""
Hydraulic Fracturing Water-Energy scenarios generator


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

from . import type_curves
from . import parameters
from . import wells_zone
from . import model

