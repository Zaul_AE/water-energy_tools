# -*- coding: utf-8 -*-
"""
Zones generator


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

import numpy as np
import pandas as pd
import geopandas as gpd
from datetime import datetime
from scipy.stats import norm, gamma, exponweib
from scipy.optimize import newton
from scipy.special import gamma as fgamma

from copy import deepcopy

import type_curves as tc
import flowback_production as fpc
from parameters import *

TYPE_CURVES = {
    'exponential': tc.ExponentialCurve(),
    'harmonic': tc.HarmonicCurve(),
    'hyperbolic': tc.HyperbolicCurve(),
}

# TODO: parameters validation
"""
DEFAULT_PARAMETERS = {
    'development': {
        'maximum_density': 0.,
        'wells_prediction': dict.fromkeys(('coef', 'price_exp', 'wells_exp'), 0.)
    },
    'production': (dict.fromkeys(('init_production', 'decline_rate', 'decline_exponent'), 0)
                   .update(dict(type_curve='hyperbolic'))),
    'water': dict.fromkeys(('water', 'length', 'proppant', 'fpwater', 'production', 'fpwater_recycled'), 0),
    'wells': (dict.fromkeys(('life_span', 'lateral_separation', 'drilling_water', 'drilling_time',
                             'fracturing_time'), 0).update(dict(consecutive_drilling=True)))
}
"""

PARAMETERS = dict([
    ('type_curve', 'Type Curve'),
    ('decline_rate', 'd'),
    ('decline_exponent', 'b'),
    ('init_production', 'Q0'),
    ('water', 'Water'),
    ('length', 'Water/Length'),
    ('proppant', 'Proppant/Water'),
    ('fpwater', 'FPwater/Water'),
    ('production', 'Production/Water'),
    ('fpwater_recycled', 'FP recycled'),
    ('fpwater_d', 'FP curve d'),
    ('fpwater_b', 'FP curve b'),
    ('life_span', 'Life span'),
    ('lateral_separation', 'Lateral separation'),
    ('drilling_water', 'Drilling Water'),
    ('drilling_time', 'Drilling time'),
    ('fracturing_time', 'Fracturing time'),
])


# =============================================================================
# Zone wells generator
# =============================================================================

class WellsGenerator(object):
    def __init__(self, params, blocks):
        # set parameters
        self.development = params['development']
        self.parameters = params['parameters']
        self.trends = params.get('trends', None)

        # set blocks
        self.blocks = blocks[['Id', 'area', 'weight']]  # [block id, area in km2, weight value]
        self.blocks['prob'] = self.blocks['weight'] / self.blocks['weight'].sum()
        self.blocks['x'] = blocks.centroid.x
        self.blocks['y'] = blocks.centroid.y

        # set production type curve and flowback-production curve
        self.type_curve = TYPE_CURVES[params['parameters']['type_curve'].lower()]
        self.fpcurve = fpc.FPwaterCurve()

        self.model = True

    def __repr__(self):
        return 'WellsGenerator()'

    def increase_parameters(self, year):
        """Use trends to increase parameters for wells generation"""
        if self.trends is not None:
            if self.trends.get('until', 10000) > year:
                for key in self.trends:
                    if key != 'until':
                        self.parameters[key] = increase_parameters(
                            self.parameters[key],
                            self.trends[key]
                        )

    def project_wells_number(self, price, nwells):
        """
        Wells projection for current year

        :param price:       [float] current hydrocarbon price
        :param nwells:      [int] last year number of wells
        :return:            [int] current number of wells
        """

        if nwells == 0:
            return int(self.development['firstyear_wells'])
        else:
            predict = int(self.development['wells_prediction']['coef']
                          * price ** self.development['wells_prediction']['price_exp']
                          * nwells ** self.development['wells_prediction']['wells_exp'])
            minwells = int(self.development['minimum_wells'])

            return max(predict, minwells)

    def random_wells(self, n):
        """
        Generate a sample of n random wells with the characteristics of the zone

        :param n:    [int] number of wells
        :return:     [DataFrame] wells sample with multiple attributes
        """

        # Random parameters
        well_seed = dict.fromkeys(PARAMETERS.values())
        for key, value in PARAMETERS.items():
            well_seed[value] = get_parameters(self.parameters[key], n)
        wells = pd.DataFrame(well_seed)

        # Aditional parameters
        wells['Drilling Water'] = wells['Drilling Water'] * wells['Water'] 
        wells['Production'] = wells['Production/Water'] * wells['Water']
        wells['Length'] = wells['Water'] / wells['Water/Length']
        wells['Proppant'] = wells['Proppant/Water'] * wells['Water']
        wells['FPwater'] = wells['FPwater/Water'] * wells['Water']
        wells['FPvol recycled'] = wells['FP recycled'] * wells['FPwater']
        wells['Well Area'] = wells['Length'] * wells['Lateral separation']
        wells['Q0'] = wells['Q0'] * wells['Production']

        return wells

    def associate_blocks(self, wells):
        """
        Associate wells to blocks ID

        :param wells:   [DataFrame]
        :return:
        """
        nwells = wells.shape[0]
        blocks_id = np.random.choice(self.blocks['Id'].values, nwells,
                                     p=self.blocks['prob'].values)

        blocks_id = pd.DataFrame(blocks_id, columns=['Block'], index=wells.index)
        wells = blocks_id.join(wells)

        return wells

    def update_blocks_probabilities(self, total_wells):
        """Update blocks probabilities of new wells drilling"""
        if total_wells.shape[0] >= self.development['total_wells']:
            self.blocks['prob'] = 0
        else:
            wells = total_wells[['Block', 'Well Area']].groupby('Block').agg(['count', 'sum'])['Well Area']
            wells.columns = ['wells', 'wells area']
            wells['wells area'] /= 1e6
            wells_density = (self.blocks[['Id', 'area', 'weight', 'prob']]
                             .merge(wells, left_on='Id', right_index=True, how='left'))
            wells_density['density'] = wells_density['wells'] / wells_density['area']
            wells_density['covered'] = wells_density['wells area'] / wells_density['area']

            mask = ((wells_density['density'] >= self.development['maximum_welldensity'])
                    | (wells_density['covered'] >= self.development['area_cover']))
            self.blocks.loc[mask.values, 'weight'] = 0
            self.blocks['prob'] = self.blocks['weight'] / self.blocks['weight'].sum()

    def check_simulation_state(self):
        """When no more wells can be drilled model turns False"""
        if self.blocks['prob'].sum() == 0:
            self.model = False

    def production_simulation(self, wells, end_date):
        """Hydrocarbon production by block"""

        # Fix dates
        end_date = pd.Timestamp(end_date)

        nblocks = self.blocks.shape[0]
        start_date = wells['StartProduction'].min()
        dates = (pd.date_range(start=start_date, end=end_date, freq='1M')
                 + pd.offsets.MonthBegin(-1))

        # production by blocks
        production = pd.DataFrame(np.zeros((len(dates), nblocks)),
                                  columns=self.blocks['Id'],
                                  index=dates)
        production_acum = production.copy()

        for i in range(wells.shape[0]):
            well = wells.iloc[i, :]

            start_index = np.where(dates == well['StartProduction'])[0][0]
            if end_date > well['EndProduction']:
                end_index = np.where(dates == well['EndProduction'])[0][0]
            else:
                end_index = len(dates) - 1

            simul_dates = dates[start_index:end_index+1]
            nmonths = len(simul_dates)
            if well['Type Curve'] == 'hyperbolic':
                self.type_curve.set_parameters(d0=well['d'], b=well['b'])
            else:
                self.type_curve.set_parameters(d=well['d'])

            q12 = well['Production'] - well['Q0']   # cumulative decline curve
            q0 = self.type_curve.initial_production(q12, 12)

            prod = self.type_curve.production(np.arange(1, nmonths), q0=q0)
            prod_acum = self.type_curve.cumulative_production(np.arange(1, nmonths), q0=q0) + well['Q0']

            # Initial production
            prod = np.append(well['Q0'], prod)
            prod_acum = np.append(well['Q0'], prod_acum)

            prod = pd.Series(prod, index=simul_dates)
            prod_acum = pd.Series(prod_acum, index=simul_dates)
            prod_acum_fix = pd.Series(np.zeros_like(dates, dtype=float), index=dates)
            prod_acum_fix = prod_acum_fix.add(prod_acum, fill_value=0)
            prod_acum_fix[dates > simul_dates.max()] = prod_acum.max()

            production.loc[:, well['Block']] = production[well['Block']].add(prod, fill_value=0)
            production_acum.loc[:, well['Block']] = production_acum[well['Block']].add(prod_acum_fix, fill_value=0)

        return wells, production, production_acum

    def fpwater_simulation(self, wells, end_date):
        """Flowback and produced water by block"""

        # Fix dates
        end_date = pd.Timestamp(end_date)
        mask = wells['EndProduction'] > end_date
        wells.loc[mask, 'EndProduction'] = end_date

        nblocks = self.blocks.shape[0]
        start_date = wells['StartFracturing'].min()
        dates = (pd.date_range(start=start_date, end=end_date, freq='1M')
                 + pd.offsets.MonthBegin(-1))

        # fpwater simulation
        fpwater = pd.DataFrame(np.zeros((len(dates), nblocks)),
                               columns=self.blocks['Id'],
                               index=dates)
        fprecycled = fpwater.copy()

        for i in range(wells.shape[0]):
            well = wells.iloc[i, :]

            start_index = np.where(dates == well['StartFracturing'])[0][0]
            if end_date > well['EndProduction']:
                end_index = np.where(dates == well['EndProduction'])[0][0]
            else:
                end_index = len(dates) - 1

            simul_dates = dates[start_index:end_index + 1]
            nmonths = len(simul_dates)

            self.fpcurve.set_parameters(d0=well['FP curve d'], b=well['FP curve b'])

            fp0 = self.fpcurve.initial_fpwater(well['FPwater'], 12)
            fpw = self.fpcurve.fpwater(np.arange(1, nmonths + 1), q0=fp0)
            fpw = pd.Series(fpw, index=simul_dates)
            fpr = fpw * well['FP recycled']

            fpwater.loc[:, well['Block']] = fpwater[well['Block']].add(fpw, fill_value=0)
            fprecycled.loc[:, well['Block']] = fprecycled[well['Block']].add(fpr, fill_value=0)

        return fpwater, fprecycled

    def recycle_fpwater(self, total_wells, fprecycled):

        start_date = fprecycled.index.min()
        end_date = fprecycled.index.max()
        dates = (pd.date_range(start=start_date, end=end_date, freq='1M')
                 + pd.offsets.MonthBegin(-1))

        total_wells['FreshWater'] = 0

        if self.development['share_recycled_water']:   # share water between blocks
            fprecycled_acum = fprecycled.sum(axis=1)
            fpvol = 0
            for i, date in enumerate(dates):
                if i > 0:
                    fpvol += fprecycled_acum.iloc[i-1]

                mask = total_wells['StartFracturing'] == date
                nwells = mask.sum()
                if nwells > 0:
                    water_vol = total_wells.loc[mask, 'Water'].sum()
                    if water_vol > fpvol:
                        ratio = 1 - fpvol / water_vol
                        fpvol = 0
                    else:
                        ratio = 1
                        fpvol = fpvol - water_vol
                    total_wells.loc[mask, 'FreshWater'] = total_wells.loc[mask, 'Water'] * ratio
        else:                                          # water on the same zone
            for zone in fprecycled.columns:
                fprecycled_acum = fprecycled.loc[:, zone]
                fpvol = 0
                for i, date in enumerate(dates):
                    if i > 0:
                        fpvol += fprecycled_acum.iloc[i - 1]

                    mask = (total_wells['StartFracturing'] == date) & (total_wells['Block'] == zone)
                    nwells = mask.sum()
                    if nwells > 0:
                        water_vol = total_wells.loc[mask, 'Water'].sum()
                        if water_vol > fpvol:
                            ratio = 1 - fpvol / water_vol
                            fpvol = 0
                        else:
                            ratio = 1
                            fpvol = fpvol - water_vol
                        total_wells.loc[mask, 'FreshWater'] = total_wells.loc[mask, 'Water'] * ratio

        total_wells = total_wells.sort_values(by=['StartDrilling'])
        total_wells.reset_index(inplace=True)
        return total_wells

    @staticmethod
    def associate_dates(wells, year):
        nwells = wells.shape[0]
        wells['Year'] = year
        start_date = pd.DataFrame(np.ones((nwells, 3), dtype=int),
                                  columns=['Year', 'Month', 'Day'],
                                  index=wells.index)
        start_date['Year'] = year
        start_date['Month'] = np.random.choice(np.arange(1, 13, dtype=int), nwells)
        wells['StartDrilling'] = pd.to_datetime(start_date)

        wells['StartFracturing'] = [wells['StartDrilling'].iloc[i] +
                                    pd.offsets.MonthBegin(wells['Drilling time'].iloc[i])
                                    for i in range(nwells)]

        wells['StartProduction'] = [wells['StartFracturing'].iloc[i] +
                                    pd.offsets.MonthBegin(wells['Fracturing time'].iloc[i])
                                    for i in range(nwells)]

        wells['EndProduction'] = [wells['StartProduction'].iloc[i] +
                                  pd.offsets.MonthEnd(wells['Life span'].iloc[i] * 12)
                                  + pd.offsets.MonthBegin(-1)
                                  for i in range(nwells)]

        return wells
