# -*- coding: utf-8 -*-
"""
Flowback and produced water curves


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

import numpy as np
from scipy.optimize import minimize


# =============================================================================
# Curve model
# =============================================================================

class FPwaterCurve(object):
    def __init__(self, d0=0.2, b=0.1):
        """
        Flowback and produced water type curve

        :param d0:    [float] input initial decline rate
        :param b:     [float] input decline rate
        """
        self.d0 = d0
        self.b = b

    def __repr__(self):
        return 'FPwaterCurve()'

    def __getitem__(self, key):
        return self.__dict__.get(key, None)

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def set_parameters(self, **kargs):
        """Set parameters"""
        if 'd0' in kargs:
            self.d0 = kargs['d0']
        elif 'decline_rate' in kargs:
            self.d0 = kargs['decline_rate']
        if 'b' in kargs:
            self.b = kargs['b']
        elif 'decline_exponent' in kargs:
            self.b = kargs['decline_exponent']

    def get_parameters(self):
        """Get parameters as dictionary"""
        return self.__dict__

    def fpwater(self, t, q0=1.0, t0=1.0):
        """
        Flowback and Produced water simulation

        :param t:       [float, darray] time to evaluate (months)
        :param q0:      [float] initial production
        :param t0:      [float] initial time, associated to q0
        :return:        [as t] fpwater simulated
        """
        d0, b = self.d0, self.b
        return q0 * (1. + d0 * b * (t - t0)) ** (-1. / b)

    def cumulative_fpwater(self, t, q0=1.0, t0=1.0):
        """
        Cumulative Flowback and Produced water simulation

        :param t:     [float, ndarray] time to evaluate
        :param q0:    [float] initial production
        :param t0:    [float] initial time, associated to q0
        :return:      [as t] fpwater simulated
        """
        d0, b = self.d0, self.b
        return q0 / (d0 * (1. - b)) * (1 - (1. + d0 * b * (t - t0)) ** (1. - 1. / b))

    def initial_fpwater(self, qacum, t, t0=1):
        """
        Returns the initial production q0 from cumulative production and time

        :param qacum: [float] cumulative production associated to time t
        :param t:     [float] time to evaluate
        :param t0:    [float] initial time, associated to q0
        :return:      [float] initial production q0
        """
        d0, b = self.d0, self.b
        return qacum * (d0 * (1. - b)) / (1 - (1. + d0 * b * (t - t0)) ** (1. - 1. / b))

    def fit(self, obs, x0=None):
        """
        Fit Exponential type curve given input observations

        :param obs:      [ndarray] input observation array [[t0, q0], [t1, q1],...]
                            time in years, q is the annual accumulation
        :param x0:       [list] initial decline rate. If None, x0 is set as [0.2, 0.1]
        """
        if x0 is None:
            x0 = [0.2, 0.1]

        def funobj(param):
            d0, b = param
            t = obs[:, 0] * 12
            qacum = obs[0, 1]
            q0 = qacum * (d0 * (1. - b)) / (1 - (1. + d0 * b * (12 - 1)) ** (1. - 1. / b))
            q_acum = q0 / (d0 * (1. - b)) * (1 - (1. + d0 * b * (t - 1)) ** (1. - 1. / b))
            q_simul = q_acum.copy()
            q_simul[1:] = q_simul[1:] - q_simul[:-1]

            error = np.sum(np.abs((q_simul - obs[:, 1])))
            return error

        result = minimize(funobj, x0, method='nelder-mead',
                          options={'xtol': 1e-8, 'disp': False})

        self.d0 = result.x[0]
        self.b = result.x[1]
