# -*- coding: utf-8 -*-
"""
Oil and Gas Well TypeCurves

Types curves simulated production and cumulative production in a month time-step
Cumulative production is the sum of all months since an initial time t0

Classes
    ExponentialCurve
    HyperbolicCurve
    HarmonicCurve


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

import numpy as np
from scipy.optimize import minimize


# =============================================================================
# Exponential type curve model
# =============================================================================

class ExponentialCurve(object):
    def __init__(self, d=0.2):
        """
        Exponential Type Curve

        :param d:     [float] input decline rate
        """
        self.d = d

    def __repr__(self):
        return 'ExponentialCurve()'

    def __getitem__(self, key):
        return self.__dict__.get(key, None)

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def set_parameters(self, **kargs):
        """Set parameters"""
        if 'd' in kargs:
            self.d = kargs['d']
        elif 'decline_rate' in kargs:
            self.d = kargs['decline_rate']

    def get_parameters(self):
        """Get parameters as dictionary"""
        return self.__dict__

    def production(self, t, q0=1.0, t0=1.0):
        """
        Exponential Type Curve Simulation

        :param t:     [float, ndarray] time to evaluate
        :param q0:    [float] initial production
        :param t0:    [float] initial time, associated to q0
        :return:      [as t] production simulated
        """
        d = self.d
        return q0 * np.exp(-d * (t - t0))

    def cumulative_production(self, t, q0=1.0, t0=1.0):
        """
        Cumulative Exponential Type Curve Simulation

        :param t:     [float, ndarray] time to evaluate
        :param q0:    [float] initial production
        :param t0:    [float] initial time, associated to q0
        :return:      [as t] production simulated
        """
        d = self.d
        return (q0 / d) * (1. - np.exp(-d * (t - t0)))

    def initial_production(self, qacum, t, t0=1):
        """
        Returns the initial production q0 from cumulative production and time

        :param qacum: [float] cumulative production associated to time t
        :param t:     [float] time to evaluate
        :param t0:    [float] initial time, associated to q0
        :return:      [float] initial production q0
        """
        d = self.d
        return qacum * d / (1. - np.exp(-d * (t - t0)))

    def fit(self, obs, x0=0.2):
        """
        Fit Exponential type curve given input observations

        :param obs:      [ndarray] input observation array [[t0, q0], [t1, q1],...]
        :param x0:       [float] initial decline rate
        """
        def funobj(d, t, q0=1.0, t0=1.0):
            return q0 * np.exp(-d * (t - t0))

        result = _optimize(funobj, obs, x0)
        self.d = result.x[0]


# =============================================================================
# Hyperbolic type curve model
# =============================================================================

class HyperbolicCurve(object):
    def __init__(self, d0=0.2, b=0.1):
        """
        Hyperbolic Type Curve

        :param d0:    [float] input initial decline rate
        :param b:     [float] input decline rate
        """
        self.d0 = d0
        self.b  = b

    def __repr__(self):
        return 'HyperbolicCurve()'

    def __getitem__(self, key):
        return self.__dict__.get(key, None)

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def set_parameters(self, **kargs):
        """Set parameters"""
        if 'd0' in kargs:
            self.d0 = kargs['d0']
        elif 'decline_rate' in kargs:
            self.d0 = kargs['decline_rate']
        if 'b' in kargs:
            self.b = kargs['b']
        elif 'decline_exponent' in kargs:
            self.b = kargs['decline_exponent']

    def get_parameters(self):
        """Get parameters as dictionary"""
        return self.__dict__

    def production(self, t, q0=1.0, t0=1.0):
        """
        Hyperbolic Type Curve Simulation

        :param t:       [float, darray] time to evaluate
        :param q0:      [float] initial production
        :param t0:      [float] initial time, associated to q0
        :return:        [as t] production simulated
        """
        d0, b = self.d0, self.b
        return q0 * (1. + d0 * b * (t - t0)) ** (-1. / b)

    def cumulative_production(self, t, q0=1.0, t0=1.0):
        """
        Cumulative Exponential Type Curve Simulation

        :param t:     [float, ndarray] time to evaluate
        :param q0:    [float] initial production
        :param t0:    [float] initial time, associated to q0
        :return:      [as t] production simulated
        """
        d0, b = self.d0, self.b
        return q0 / (d0 * (1. - b)) * (1 - (1. + d0 * b * (t - t0)) ** (1. - 1. / b))

    def initial_production(self, qacum, t, t0=1):
        """
        Returns the initial production q0 from cumulative production and time

        :param qacum: [float] cumulative production associated to time t
        :param t:     [float] time to evaluate
        :param t0:    [float] initial time, associated to q0
        :return:      [float] initial production q0
        """
        d0, b = self.d0, self.b
        return qacum * (d0 * (1. - b)) / (1 - (1. + d0 * b * (t - t0)) ** (1. - 1. / b))

    def fit(self, obs, x0=None):
        """
        Fit Exponential type curve given input observations

        :param obs:      [ndarray] input observation array [[t0, q0], [t1, q1],...]
        :param x0:       [list] initial decline rate. If None, x0 is set as [0.2, 0.1]
        """
        if x0 is None:
            x0 = [0.2, 0.1]

        def funobj(param, t, q0=1.0, t0=1.0):
            return q0 * (1. + param[0] * param[1] * (t - t0)) ** (-1. / param[1])

        result = _optimize(funobj, obs, x0)
        self.d0 = result.x[0]
        self.b  = result.x[1]


# =============================================================================
# Harmonic type curve model
# =============================================================================

class HarmonicCurve(object):
    def __init__(self, d=0.2):
        """
        Harmonic Type Curve

        :param d:     [float] input decline rate
        """
        self.d = d

    def __repr__(self):
        return 'HarmonicCurve()'

    def __getitem__(self, key):
        return self.__dict__.get(key, None)

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def set_parameters(self, **kargs):
        """Set parameters"""
        if 'd' in kargs:
            self.d = kargs['d']
        elif 'decline_rate' in kargs:
            self.d = kargs['decline_rate']

    def get_parameters(self):
        """Get parameters as dictionary"""
        return self.__dict__

    def production(self, t, q0=1.0, t0=1.0):
        """
        Exponential Type Curve Simulation

        :param t:     [float, ndarray] time to evaluate
        :param q0:    [float] initial production
        :param t0:    [float] initial time, associated to q0
        :return:      [as t] production simulated
        """
        d = self.d
        return q0 / (1. + d * (t - t0))

    def cumulative_production(self, t, q0=1.0, t0=1.0):
        """
        Cumulative Harmonic Type Curve Simulation

        :param t:     [float, ndarray] time to evaluate
        :param q0:    [float] initial production
        :param t0:    [float] initial time, associated to q0
        :return:      [as t] production simulated
        """
        d = self.d
        return q0 / d * np.log(1 + d * (t - t0))

    def initial_production(self, qacum, t, t0=1):
        """
        Returns the initial production q0 from cumulative production and time

        :param qacum: [float] cumulative production associated to time t
        :param t:     [float] time to evaluate
        :param t0:    [float] initial time, associated to q0
        :return:      [float] initial production q0
        """
        d = self.d
        return qacum * d / np.log(1 + d * (t - t0))

    def fit(self, obs, x0=0.2):
        """
        Fit Harmonic type curve given input observations

        :param obs:      [ndarray] input observation array [[t0, q0], [t1, q1],...]
        :param x0:       [float] initial decline rate
        """
        def funobj(d, t, q0=1.0, t0=1.0):
            return q0 / (1. + d * (t - t0))

        result = _optimize(funobj, obs, x0)
        self.d = result.x[0]


# =============================================================================
# Optimization function
# =============================================================================

def _objective_function(func, obs):
    """
    Creates an objective function given an input simulation function and
    observation data. Output function uses sum of quadratic difference
    :param func:     input type curve function
    :param obs:      [ndarray] input observation array [[t0, q0], [t1, q1],...]
    :return:
    """
    t0, q0 = obs[0, :]
    yobs = obs[1:, 1]

    def obj_fun(param):
        return np.sum((yobs - func(param, t=obs[1:, 0], q0=q0, t0=t0)) ** 2.)

    return obj_fun


def _optimize(func, obs, x0):
    """
    Objective Function Optimization for type curves fit

    :param func:     input type curve function
    :param obs:      [ndarray] input observation array [[t0, q0], [t1, q1],...]
    :param x0:       [float, list, ndarray] initial parameter
    :return:         [object] output minimize structure
    """
    objective = _objective_function(func, obs)
    result = minimize(objective, x0, method='nelder-mead',
                      options={'xtol': 1e-8, 'disp': False})
    return result
