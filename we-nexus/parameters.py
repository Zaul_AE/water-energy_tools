# -*- coding: utf-8 -*-
"""
Model Parameters


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

import numpy as np
from scipy.stats import norm, gamma, exponweib
from scipy.optimize import newton
from scipy.special import gamma as fgamma


# =============================================================================
# Parameters
# =============================================================================

def get_parameters(value, n):
    """
    Returns sample of given value an a number of data

    :param value:   [float, int, list, ndarray, distribution]
                      input type of value
    :param n:       [int] number of elements to generate
    :return:        [array] 1D array of random sample
    """
    if type(value) is str:
        sample = np.full(n, value, dtype=object)
    elif type(value) in (int, float):
        sample = np.full(n, value)
    elif type(value) in (tuple, list, np.ndarray):
        if type(value) is not np.ndarray:
            value = np.array(value, dtype=float)
        pos = np.random.randint(0, len(value), n)
        sample = value[pos]
    elif type(value) is dict:
        dtype = value['dist']
        m = float(value['mean'])
        s = float(value['std'])
        # Fit distribution
        if dtype == 'norm':
            dist = norm(loc=m, scale=s)
        elif dtype == 'gamma':
            a = (m / s) ** 2.0
            b = s ** 2.0 / m
            dist = gamma(a, scale=b)
        elif dtype == 'weibull':
            def func(x):
                res = np.log(fgamma(1 + 2. / x)) - 2 * np.log(fgamma(1 + 1. / x))
                res -= np.log(s ** 2 + m ** 2) + 2 * np.log(m)
            b = newton(func, 1.0)
            a = m / fgamma(1. + 1. / b)
            dist = exponweib(1., b, scale=a)
        sample = dist.rvs(size=n)
    else:
        raise TypeError('Wrong input parameter type')

    return sample


def increase_parameters(value, increase):
    """
    Set an increase or decrease percentage to the input value.
    If the input value is a statistic distribution, mean and std
    can be affected individually

    :param value:       [float, ndarray, distribution] input object
    :param increase:    [float, dict] when input value is float or an array
                         increase is a float value in the range -inf < increase < inf
                         When the input value is a distribution, increase is a
                         dictionary with {'mean': mean_increase, 'std': std_increase}
    :return:             increased value or statistic distribution with new parameters
    """
    if type(value) in (int, float) and type(increase) in (int, float):
        value += increase
    elif type(value) in (tuple, list, np.ndarray) and type(increase) in (int, float):
        value = [x + increase for x in value]
    elif type(value) is dict and type(increase) is dict:
        value['mean'] += increase.get('mean', 0)
        value['std'] += increase.get('std', 0)
    else:
        raise TypeError('Wrong increase or value data type')
    return value
