# -*- coding: utf-8 -*-
"""
Hydraulic Fracturing Water-Energy scenarios generator


Author:
    Saul Arciniega Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

    contact:
    zaul.ae@gmail.com
    sarciniegae@ii.unam.mx
"""

import os
import toml
import numpy as np
import pandas as pd
import geopandas as gpd
import shutil
from copy import deepcopy

from wells_zone import WellsGenerator

import warnings
warnings.filterwarnings('ignore')
warnings.simplefilter(action='ignore', category=FutureWarning)


# =============================================================================
# Main routine for HF Water-Energy scenarios
# =============================================================================

def run(parameters):

    # Check inputs
    if type(parameters) is str:
        if os.path.exists(parameters):
            with open(parameters, 'r') as fid:
                parameters = toml.load(fid)
        else:
            raise IOError(f'File name < {parameters} does not exist!>')
    if type(parameters) is not dict:
        raise TypeError('Wrong parameters type')

    save_params = deepcopy(parameters)

    # Load data
    simulation = parameters.pop('simulation')
    layer = gpd.read_file(simulation['polygons_layer'])
    prices = pd.read_csv(simulation['prices_predictions'], index_col=[0])

    dates = (pd.date_range(start=simulation['start_date'], end=simulation['end_date'], freq='1M')
             + pd.offsets.MonthBegin(-1))
    start_year = dates.year.min()
    end_year = dates.year.max()

    root_folder = os.path.join(simulation['save_results'], simulation['title'])
    if os.path.exists(root_folder):
        if simulation['overwrite']:
            shutil.rmtree(root_folder)
            os.makedirs(root_folder)
        else:
            raise IOError(f'Output folder {root_folder} already_exist!')
    else:
        os.makedirs(root_folder)

    # Create zones
    zones_keys = list(parameters.keys())
    zones = dict.fromkeys(zones_keys)
    wells = dict.fromkeys(zones_keys)
    production = dict.fromkeys(zones_keys)
    cum_production = dict.fromkeys(zones_keys)
    fpwater = dict.fromkeys(zones_keys)
    fprecycled = dict.fromkeys(zones_keys)

    for key in zones_keys:
        layer_zone = layer.loc[layer['zone'] == key, :]
        nblocks = layer_zone.shape[0]
        if nblocks == 0:
            raise ValueError(f'Polygons layer < {simulation["polygons_layer"]} > '
                             f'does not contain any < {key} > zone')

        zones[key] = WellsGenerator(parameters[key], layer_zone)
        production[key] = pd.DataFrame(np.zeros((len(dates), nblocks)),
                                       columns=layer_zone['Id'],
                                       index=dates)
        cum_production[key] = pd.DataFrame(np.zeros((len(dates), nblocks)),
                                           columns=layer_zone['Id'],
                                           index=dates)
        fpwater[key] = pd.DataFrame(np.zeros((len(dates), nblocks)),
                                    columns=layer_zone['Id'],
                                    index=dates)
        fprecycled[key] = pd.DataFrame(np.zeros((len(dates), nblocks)),
                                       columns=layer_zone['Id'],
                                       index=dates)

        if key not in prices:
            raise KeyError(f'Prices time series < {simulation["prices_predictions"]} > '
                           f'does not contain the zone < {key} >')

    # Main Loop
    print('\n# Hydraulic Fracturing Water-Energy Nexus Scenarios Generator #')
    for key in zones_keys:
        print(f'\nModelling zone:{key}____________________________________________')
        nwells = 0
        for i, year in enumerate(range(start_year, end_year+1)):
            print(f'Year: {year}')
            # Try to increase parameters
            if i > 0:
                zones[key].increase_parameters(year)

            if zones[key].model:
                # Create new wells
                new_wells = zones[key].project_wells_number(prices.loc[year, key], nwells)
                random_wells = zones[key].random_wells(new_wells)
                random_wells = zones[key].associate_blocks(random_wells)
                random_wells = zones[key].associate_dates(random_wells, year)
                random_wells['Zone'] = key
                if i > 0:
                    current_wells = wells[key].shape[0]
                    new_wells = random_wells.shape[0]
                    total_wells = current_wells + new_wells

                    if total_wells > zones[key].development['total_wells']:
                        keep_wells = zones[key].development['total_wells'] - current_wells
                        random_wells = random_wells.iloc[:keep_wells, :]

                # Remove last wells
                if year == end_year:
                    mask = random_wells['StartProduction'] < simulation['end_date']
                    random_wells = random_wells[mask]

                # Wells production
                random_wells, prod, prod_acum = zones[key].production_simulation(
                    random_wells,
                    simulation['end_date']
                )

                # Flowback and produced water
                fpw, fpr = zones[key].fpwater_simulation(
                    random_wells,
                    simulation['end_date']
                )

                # Acumulate all results
                if i == 0:
                    wells[key] = random_wells
                else:
                    wells[key] = pd.concat((wells[key], random_wells))

                production[key] = production[key].add(prod, fill_value=0)
                cum_production[key] = cum_production[key].add(prod_acum, fill_value=0)
                fpwater[key] = fpwater[key].add(fpw, fill_value=0)
                fprecycled[key] = fprecycled[key].add(fpr, fill_value=0)

                nwells = random_wells.shape[0]

                zones[key].update_blocks_probabilities(wells[key])
                zones[key].check_simulation_state()

        # Recycled water and Fresh water
        wells[key] = zones[key].recycle_fpwater(wells[key], fprecycled[key])
        wells['Zone'] = key

    print('\nExporting Results____________________________________________')
    print('Note: annual and cummulative results was divided by 1e6')

    with open(os.path.join(root_folder, 'params.toml'), 'w') as fout:
        toml.dump(save_params, fout)

    # Export results by zones
    print('\nResults by zone...')
    folder_out = os.path.join(root_folder, 'ByZone')
    os.makedirs(folder_out)
    for key in zones_keys:
        saveas = os.path.join(folder_out, f'{key}_Wells.csv')
        wells[key].to_csv(saveas, index=False)

        saveas = os.path.join(folder_out, f'{key}_Production.csv')
        production[key].to_csv(saveas)

        saveas = os.path.join(folder_out, f'{key}_Production_cummulative.csv')
        cum_production[key].to_csv(saveas)

        saveas = os.path.join(folder_out, f'{key}_FPwater.csv')
        fpwater[key].to_csv(saveas)

    # All zones
    print('Results by all zones...')
    folder_out = os.path.join(root_folder, 'AllZones')
    os.makedirs(folder_out)
    for i, key in enumerate(zones_keys):
        if i == 0:
            all_wells = wells[key]
            all_prod = production[key].sum(axis=1) / 1e6
            all_prod_acum = cum_production[key].sum(axis=1) / 1e6
            all_flowback = fpwater[key].sum(axis=1) / 1e6
        else:
            all_wells = pd.concat((all_wells, wells[key]))
            all_prod = pd.concat((all_prod, production[key].sum(axis=1) / 1e6), axis=1)
            all_prod_acum = pd.concat((all_prod_acum, cum_production[key].sum(axis=1) / 1e6), axis=1)
            all_flowback = pd.concat((all_flowback, fpwater[key].sum(axis=1) / 1e6), axis=1)

    if type(all_prod) is type(pd.Series()):
        all_prod = all_prod.to_frame()
    if type(all_prod_acum) is type(pd.Series()):
        all_prod_acum = all_prod_acum.to_frame()
    if type(all_flowback) is type(pd.Series()):
        all_flowback = all_flowback.to_frame()

    all_prod.columns = zones_keys
    all_prod_acum.columns = zones_keys
    all_flowback.columns = zones_keys

    all_wells.to_csv(os.path.join(folder_out, 'Wells.csv'), index=False)
    all_prod.to_csv(os.path.join(folder_out, 'Production.csv'))
    all_prod_acum.to_csv(os.path.join(folder_out, 'Production_cummulative.csv'))
    all_flowback.to_csv(os.path.join(folder_out, 'FPwater.csv'))

    # Export annual results
    print('Results by year...')
    folder_out = os.path.join(root_folder, 'ByYear')
    os.makedirs(folder_out)

    year_wells = all_wells[['Year', 'Water', 'FreshWater', 'Drilling Water']].groupby('Year').sum() / 1e6
    year_wells['Wells'] = all_wells[['Year', 'Water']].groupby('Year').count()

    keys = ['Year', 'Water', 'FreshWater', 'Drilling Water', 'Proppant', 'Length', 'FPwater', 'Production']
    year_wells_stat = all_wells[keys].groupby('Year').mean()
    year_wells_stat['Wells'] = all_wells[['Year', 'Water']].groupby('Year').count()

    year_prod = all_prod.groupby(all_prod.index.year).sum()
    year_prod_cum = all_prod_acum.groupby(all_prod_acum.index.year).sum()
    year_flow_back = all_flowback.groupby(all_flowback.index.year).sum()

    year_wells.to_csv(os.path.join(folder_out, 'Water.csv'))
    year_wells_stat.to_csv(os.path.join(folder_out, 'Wells_means.csv'))
    year_prod.to_csv(os.path.join(folder_out, 'Production.csv'))
    year_prod_cum.to_csv(os.path.join(folder_out, 'Production_cummulative.csv'))
    year_flow_back.to_csv(os.path.join(folder_out, 'FPwater.csv'))

    # Polygons statistics
    print('Results by blocks...')
    folder_out = os.path.join(root_folder, 'ByBlocks')
    os.makedirs(folder_out)

    keys = ['Block', 'Water', 'FreshWater', 'Drilling Water', 'Proppant', 'Length', 'FPwater', 'Production']
    blocks_wells_stat = all_wells[keys].groupby('Block').mean()
    blocks_wells_stat['Wells'] = all_wells[['Block', 'Water']].groupby('Block').count()

    blocks_water = (all_wells[['Block', 'Year', 'Water']].groupby(by=['Block', 'Year'])['Water']
                    .sum().unstack(level=1)/1e6)
    blocks_water['Total'] =  blocks_water.sum(axis=1)

    blocks_fwater = (all_wells[['Block', 'Year', 'FreshWater']].groupby(by=['Block', 'Year'])['FreshWater']
                    .sum().unstack(level=1)/1e6)
    blocks_fwater['Total'] =  blocks_fwater.sum(axis=1)
    
    blocks_dwater = (all_wells[['Block', 'Year', 'Drilling Water']].groupby(by=['Block', 'Year'])['Drilling Water']
                    .sum().unstack(level=1)/1e6)
    blocks_dwater['Total'] =  blocks_fwater.sum(axis=1)

    for i, key in enumerate(zones_keys):
        if i == 0:
            block_prod = production[key]
            block_prod_acum = cum_production[key]
            block_flowback = fpwater[key]
        else:
            block_prod = pd.concat((block_prod, production[key]), axis=0)
            block_prod_acum = pd.concat((block_prod_acum, cum_production[key]), axis=0)
            block_flowback = pd.concat((block_flowback, fpwater[key]), axis=0)

    block_prod = block_prod.groupby(block_prod.index.year).sum().transpose() / 1e6
    block_prod_acum = block_prod_acum.groupby(block_prod_acum.index.year).sum().transpose() / 1e6
    block_flowback = block_flowback.groupby(block_flowback.index.year).sum().transpose() / 1e6

    block_prod['Total'] = block_prod_acum.iloc[:, -1]
    block_flowback['Total'] = block_flowback.sum(axis=1)

    blocks_wells_stat.to_csv(os.path.join(folder_out, 'Wells_mean.csv'))
    blocks_water.to_csv(os.path.join(folder_out, 'Water.csv'))
    blocks_fwater.to_csv(os.path.join(folder_out, 'FreshWater.csv'))
    blocks_dwater.to_csv(os.path.join(folder_out, 'DrillingWater.csv'))
    block_prod.to_csv(os.path.join(folder_out, 'Production.csv'))
    block_prod_acum.to_csv(os.path.join(folder_out, 'Production_cummulative.csv'))
    block_flowback.to_csv(os.path.join(folder_out, 'FPwater.csv'))

    print('\nFinished')
