# Water-Energy Nexus Scenarios generator

## Installation

OS X, Linux & Windows:

```sh
git clone https://gitlab.com/Zaul_AE/water-energy_tools.git
cd water-energy_tools
python setup.py install
```

## Dependencies

Following Python modules are required to install **datamining**:

* numpy
* scipy
* pandas
* matplotlib
* seaborn
* geopandas
* toml
* numba


## Usage example

Examples about the models are shown in Example folder.


## Release History
* 0.0.1
    * Work in progress

## Meta

Saul Arciniega Esparza – zaul.ae@gmail.com

[Twitter](https://twitter.com/zaul_arciniega) -
[Hashnode](https://hashnode.com/@ZaulAE) -
[ResearchGate](https://www.researchgate.net/profile/Saul_Arciniega-Esparza)


Distributed under the BSD-3 license. See ``LICENSE.md`` for more information.
